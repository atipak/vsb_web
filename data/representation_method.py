import rdkit
from rdkit import Chem
from rdkit import DataStructs
from rdkit.Chem.Fingerprints import FingerprintMols


# A method for representation of molecules: AtomPairFingerprint
# A method for computing of scores: FingerprintSimilarity

def representation(molecule):
    """
    Optimized method for computing a representation of a molecule.
    :param molecule: A object representing SDF description gained by library RDkit.
    :return: A representation of molecule.
    """
    fp = FingerprintMols.FingerprintMol(molecule)
    return fp

def representation(data, sdf_path):
    """
    Method for computing a representation of molecules.
    :param data: Names of all molecules in data set
    :param sdf_path: Path to a file with SDF description.
    :return: Dictionary with keys representing names of molecules and their values corresponding to representation of molecules.
    """
    repr = {}
    for molecule in rdkit.Chem.SDMolSupplier(sdf_path):
        if molecule is None:
            continue
        fp = FingerprintMols.FingerprintMol(molecule)
        repr[molecule.GetProp('_Name')] = fp
    return repr