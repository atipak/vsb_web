from rdkit import DataStructs
from rdkit.Chem.Fingerprints import FingerprintMols


# A method for representation of molecules: AtomPairFingerprint
# A method for computing of scores: FingerprintSimilarity

def similarity(active_train, inactive_train, tested_molecule, molecule_name):
    """
    A optimized method for computing a score of a molecule.
    :param active_train: Dictionary with representations of active molecules in train set (name: representation).
    :param inactive_train: Dictionary with representations of inactive molecules in train set (name: representation).
    :param tested_molecule: A representation of tested molecule.
    :param molecule_name: Name of tested molecule.
    :return:
    """
    sim = []
    for act in active_train:
        sim.append(DataStructs.FingerprintSimilarity(tested_molecule, act))
    return {
        'name': molecule_name,
        'similarity': max(sim)
    }


def similarity(repr, train, test):
    """
    A method for computing a score of molecules.
    :param repr: Dictionary with keys representing names of molecules and their values corresponding to representation of molecules.
    :param train: List of lists. First list are active molecules in train set. Second list are inactive molecules in train set.
    :param test: List of names of molecules in test set.
    :return: Dictionary with keys name of molecule and score of molecule. (keys: name, similarity)
    """
    active = train[0]
    actives = []
    for act in active:
        actives.append(repr[act])
    sim = []
    result = []
    for t in test:
        for act in actives:
            sim.append(DataStructs.FingerprintSimilarity(repr[t], act))
        result.append({
            'name': t,
            'similarity': max(sim)
        })
        sim = []
    return result
