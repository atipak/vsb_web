from flask import Flask, request, session, g, redirect, url_for, render_template, flash, send_from_directory
import os
import json
from functools import wraps
import werkzeug
import importlib
import sys
import multiprocessing


app = Flask(__name__)
app.secret_key = os.urandom(24)

PUT_SCREEN = {"method": {"type": ["Type of method:", "dropdown", "py", "general"],
                         "subtype": ["Type of python method:", "dropdown", "similarity", "representation"],
                         "path": ["Path to file:", "fileinput"]},
              "data_set": {"sdf": ["Path to a SDF file:", "fileinput"],
                           "data_set": ["Path to a data set:", "fileinput"]},
              "selection": {"data_set_id": ["ID of data set:", "form-control"],
                            "selection_path": ["Path to a selection:", "fileinput"]}}

GET_SCREEN = {"analyze": [["id", "ID of analysis", "", "1"],
                          ["subtest_id", "ID of subtest", "", "2"]],
              "method": [["id", "ID of method", "", "1"],
                         ["method_type", "Type of method", "python/general", "2"]],
              "subtest": [["id", "ID of subtest", "", "1"],
                          ["dataset_id", "ID of data set", "", "2"],
                          ["selection_id", "ID of selection", "", "3"],
                          ["test_id", "ID of test", "", "4"]],
              "molecule": [["id", "ID of molecule", "", "1"],
                           ["molecule_name", "Name of molecule", "", "2"],
                           ["subtest_id", "ID of subtest", "", "3"]],
              "file": [["id", "ID of file", "", "1"],
                       ["subtest_id", "ID of subtest", "", "2"]],
              "test": [["id", "ID of test", "", "1"],
                       ["user_id", "ID of user", "", "2"],
                       ["method_id", "ID of method", "", "3"],
                       ["test_type", "Type of test", "ls, mc, r", "4"],
                       ["launch_date", "Date of launch", "YYYY-MM-DD", "5"]]
              }

actual_tasks = {}
config = {}
i = 0
l = 0
vsb_app = None


# decorators
def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'logged_in' not in session:
            return redirect(url_for('login_site', next=request.url))
        return f(*args, **kwargs)

    return decorated_function


# main addresses

@app.route("/")
def main_site():
    return render_template('unlogged_home.html', home="You are on unlogged site")


@app.route("/logged")
def logged_site():
    return redirect("/logged/home/")


# login, registration, log out

@app.route("/login", methods=['GET', 'POST'])
def login_site():
    error = None
    if request.method == 'POST':
        # try log user in
        return_value, id = check_user(request.form['username'], request.form['password'])
        if not return_value:
            error = 'Invalid username or password'
        else:
            session['user_id'] = id
            if int(id) == 1:
                session['username'] = "host"
            else:
                session['username'] = request.form['username']
            session['logged_in'] = True
            flash('You were logged in')
            return render_template('logged_home.html', home="You are on a logged site",
                                   USERNAME=session['username'])
    return render_template('login.html', ERROR=error)


@app.route("/registration/", methods=['GET', 'POST'])
@app.route("/registration.html", methods=['GET', 'POST'])
def registration_site():
    error = None
    if request.method == 'POST':
        # try log user in
        if str(request.form['username']).strip() == "":
            error = 'Username cannot be an empty string'
            return render_template('registration.html', ERROR=error)
        if not str(request.form['password']).strip() == str(request.form['password_again']).strip():
            error = "Passwords don't match each other"
            return render_template('registration.html', ERROR=error)
        return_value, id_user = add_new_user(str(request.form['username']).strip(),
                                             str(request.form['password']).strip(),
                                             str(request.form['name']).strip(),
                                             str(request.form['last_name']).strip(),
                                             str(request.form['email']).strip())
        if not return_value:
            error = id_user
        else:
            session['user_id'] = id_user
            session['username'] = request.form['username']
            session['logged_in'] = True
            flash('You were logged in')
            return render_template('logged_home.html', home="You are on a logged site",
                                   USERNAME=session['username'])
    return render_template('registration.html', ERROR=error)


@app.route("/logout.html")
def logout_site():
    # log out
    session.pop('logged_in', None)
    return render_template('logout.html')


# logged

@app.route("/logged/home/")
@app.route("/logged/home.html")
@login_required
def logged_home_site():
    return render_template('logged_home.html', home="You are on a logged site")


# run pages

@app.route("/logged/run_ls.html", methods=['GET', 'POST'])
@login_required
def logged_run_ls():
    type = "ls"
    error = None
    if request.method == 'POST':
        # try read data from page
        dict = request.form
        if "user_id" in session:
            user_id = session["user_id"]
        else:
            user_id = 1
        b_result, d = reading_run_options(dict, request.files, user_id)
        if b_result == False:
            error = "Bad basic parameters"
            return render_template('run_ls.html', ERROR=error, TYPE=type)
        else:
            id = new_task_run(d)
            return render_template('id_of_query.html', ID=id)
    return render_template('run_ls.html', ERROR=error, TYPE=type)


@app.route("/logged/run_rc.html", methods=['GET', 'POST'])
@login_required
def logged_run_rc():
    error = None
    type = "rc"
    if request.method == 'POST':
        # try read data from page
        dict = request.form
        if "user_id" in session:
            user_id = session["user_id"]
        else:
            user_id = 1
        b_result, d = reading_run_options(dict, request.files, user_id)
        if b_result == False:
            error = "Bad basic parameters"
            return render_template('run_rc.html', ERROR=error, TYPE=type)
        else:
            b_result, d = add_rc_infos(dict, d)
            if not b_result:
                error = "Bad pcs parameters"
                return render_template('run_rc.html', ERROR=error, TYPE=type)
            else:
                id = new_task_run(d)
                return render_template('id_of_query.html', ID=id)
    return render_template('run_rc.html', ERROR=error, TYPE=type)


@app.route("/logged/run_mc.html", methods=['GET', 'POST'])
@login_required
def logged_run_mc():
    type = "mc"
    error = None
    if request.method == 'POST':
        # try read data from page
        dict = request.form
        if "user_id" in session:
            user_id = session["user_id"]
        else:
            user_id = 1
        b_result, d = reading_run_options(dict, request.files, user_id)
        if b_result == False:
            error = "Bad basic parameters"
            return render_template('run_mc.html', NODES=get_front_nodes(), ERROR=error, TYPE=type)
        else:
            b_result, d = add_metacentrum_infos(dict, d)
            if not b_result:
                error = "Bad log in parameters"
                return render_template('run_mc.html', NODES=get_front_nodes(), ERROR=error, TYPE=type)
            else:
                id = new_task_run(d)
                return render_template('id_of_query.html', ID=id)
    return render_template('run_mc.html', NODES=get_front_nodes(), ERROR=error, TYPE=type)


# put pages

@app.route("/logged/put_data_set.html", methods=['GET', 'POST'])
@login_required
def logged_put_data_set():
    if request.method == 'POST':
        data_set_f = request.files['info_file']
        sdf_f = request.files['sdf_file']
        if data_set_f and allowed_file(data_set_f.filename) and sdf_f and allowed_file(sdf_f.filename):
            data_set_path = os.path.abspath(
                os.path.join(get_temp_path(), werkzeug.secure_filename(data_set_f.filename)))
            sdf_path = os.path.abspath(os.path.join(get_temp_path(), werkzeug.secure_filename(sdf_f.filename)))
            data_set_f.save(data_set_path)
            sdf_f.save(sdf_path)
            id = new_task_put("selection", sdf_file=sdf_path, data_file=data_set_path)
            render_template('id_of_query.html', ID=id)
    with open("data/import_dataset_example.json", "r") as stream:
        example = stream.read()
    return render_template('put_data_set.html', EXAMPLE=example)


@app.route("/logged/put_method.html", methods=['GET', 'POST'])
@login_required
def logged_put_method():
    error = ""
    if request.method == 'POST':
        method_type = request.form['method_type']
        d = {}
        if method_type == "Python":
            method_type = "py"
        else:
            method_type = "general"
        d['method_type'] = method_type
        file = request.files['info_file']
        if file:
            method_path = os.path.abspath(os.path.join(get_temp_path(), werkzeug.secure_filename(file.filename)))
            file.save(method_path)
        d['file_path'] = method_path
        if method_type == "py":
            py_method_type = request.form['py_m_type_option']
            py_method_type = str(py_method_type).replace("opt_", "")
            d['py_type'] = py_method_type
        id = new_task_put("selection", **d)
        return render_template('id_of_query.html', ID=id)
    with open("data/representation_method.py", "r") as stream:
        repr = stream.read()
    with open("data/similarity_method.py", "r") as stream:
        sim = stream.read()
    with open("data/main_vs.py", "r") as stream:
        file = stream.read()
    return render_template('put_method.html', ERROR=error, SIM_EXAMPLE=sim, REPR_EXAMPLE=repr, FILE_EXAMPLE=file)


@app.route("/logged/put_selection.html", methods=['GET', 'POST'])
@login_required
def logged_put_selection():
    error = ""
    if request.method == 'POST':
        data_set_id = str(request.form['data_set_id']).strip()
        if data_set_id == "":
            error = "Insert data-set ID"
        else:
            selection_f = request.files['info_file']
            if selection_f and allowed_file(selection_f.filename):
                selection_path = os.path.abspath(
                    os.path.join(get_temp_path(), werkzeug.secure_filename(selection_f.filename)))
            selection_f.save(selection_path)
            id = new_task_put("selection", id_dataset=data_set_id, data_file=selection_path)
            render_template('id_of_query.html', ID=id)
    with open("data/import_selection_example.json", "r") as stream:
        example = stream.read()
    return render_template('put_selection.html', ERROR=error, EXAMPLE=example)


# get pages


@app.route("/logged/get_method.html", methods=['GET', 'POST'])
@login_required
def logged_get_method():
    chosen_option = "method"
    error = None
    if request.method == 'POST':
        # try read data from page
        return get_method(chosen_option)
    return render_template('get_main.html', GET_SCREEN=GET_SCREEN, chosen_option=chosen_option, ERROR=error)


@app.route("/logged/get_selection.html", methods=['GET', 'POST'])
@login_required
def logged_get_selection():
    error = None
    if request.method == 'POST':
        task_subtype = "selection"
        name_of_filter = "id_dataset"
        value_of_filter = request.form['id_dataset']
        value_of_selection_input = request.form['id_selection']
        id = new_task_get(task_subtype, name_of_filter, value_of_filter, id_selection=value_of_selection_input)
        return render_template('id_of_query.html', ID=id)
    return render_template('get_selection.html', ERROR=error)


@app.route("/logged/get_data_set.html", methods=['GET', 'POST'])
@login_required
def logged_get_data_set():
    error = None
    if request.method == 'POST':
        value = request.form['optionsRadios']
        value = str(value).replace("opt", "")
        task_subtype = "dataset"
        name_of_filter = "id_dataset"
        value_of_filter = request.form['id_dataset']
        next_filter_value = "True"
        if value == "sdf_file":
            id = new_task_get(task_subtype, name_of_filter, value_of_filter, sdf_file=next_filter_value)
        else:
            id = new_task_get(task_subtype, name_of_filter, value_of_filter, data=next_filter_value)
        return render_template('id_of_query.html', ID=id)
    return render_template('get_data_set.html', ERROR=error)


@app.route("/logged/get_analyze.html", methods=['GET', 'POST'])
@login_required
def logged_get_analyze():
    chosen_option = "analyze"
    error = None
    if request.method == 'POST':
        # try read data from page
        return get_method(chosen_option)
    return render_template('get_main.html', GET_SCREEN=GET_SCREEN, chosen_option=chosen_option, ERROR=error)


@app.route("/logged/get_subtest.html", methods=['GET', 'POST'])
@login_required
def logged_get_subtest():
    chosen_option = "subtest"
    error = None
    if request.method == 'POST':
        # try read data from page
        return get_method(chosen_option)
    return render_template('get_main.html', GET_SCREEN=GET_SCREEN, chosen_option=chosen_option, ERROR=error)


@app.route("/logged/get_molecule.html", methods=['GET', 'POST'])
@login_required
def logged_get_molecule():
    chosen_option = "molecule"
    error = None
    if request.method == 'POST':
        # try read data from page
        return get_method(chosen_option)
    return render_template('get_main.html', GET_SCREEN=GET_SCREEN, chosen_option=chosen_option, ERROR=error)


@app.route("/logged/get_test.html", methods=['GET', 'POST'])
@login_required
def logged_get_test():
    chosen_option = "test"
    error = None
    if request.method == 'POST':
        # try read data from page
        return get_method(chosen_option)
    return render_template('get_main.html', GET_SCREEN=GET_SCREEN, chosen_option=chosen_option, ERROR=error)


@app.route("/logged/get_file.html", methods=['GET', 'POST'])
@login_required
def logged_get_file():
    chosen_option = "file"
    error = None
    if request.method == 'POST':
        # try read data from page
        return get_method(chosen_option)
    return render_template('get_main.html', GET_SCREEN=GET_SCREEN, chosen_option=chosen_option, ERROR=error)


# results
@app.route("/logged/query_done.html", methods=['GET', 'POST'])
@login_required
def query_done_site():
    error = None
    if request.method == 'POST':
        # try read data from page
        id = request.form['id_text']
        value, answer = is_end_of_task(id)
        if value:
            type = get_task_information(id)[0]
            result_file = get_task_information(id)[1]
            task_subtype = get_task_information(id)[2]
            try:
                return redirect_to_result_page(id, type, result_file, task_subtype)
            except Exception as e:
                print(e)
                raise e
        else:
            error = answer
    return render_template('query_done.html', ERROR=error)


@app.route('/logged/download/', methods=['POST'])
@login_required
def download_file():
    if request.method == 'POST':
        # try read data from page
        filename = request.form['download']
        return send_from_directory(get_temp_path(), filename, as_attachment=True)


# next functions
def check_user(username, password):
    """
    Checks if a user with a given username and a given password in DB exists
    :param username: A given username
    :param password: A given password
    :return: True, id of user/False, error description
    """
    task_type = "front_end"
    task_subtype = "check_user"
    query_path = get_path(0, 'test')
    result_path = get_path(0, 'result')
    test_config = {'task_type': task_type,
                   'task_subtype': task_subtype,
                   'output_file': result_path,
                   "username": username,
                   "password": password}
    p = new_test(test_config, query_path)
    import time
    while True:
        if p.exitcode is None:
            time.sleep(0.2)
        else:
            break
    with open(result_path, "r") as stream:
        js = json.load(stream)
        if js['success']:
            id = js['data']['data_0'][0]
            return True, id
        else:
            error = js['data']['data_0']
            return False, error


def add_new_user(username, password, name, last_name, email):
    """
    Adds a new user into DB.
    :param username: A given username
    :param password: A given password
    :param name: A given name
    :param last_name: A given last name
    :param email: A given e-mail
    :return: True, user_id or False, error desription
    """
    id = new_task_put("user", login=username, name=name, last_name=last_name, email=email, password=password)
    p = get_task_information(id)[3]
    path = get_task_information(id)[1]
    import time
    while True:
        if p.exitcode is None:
            time.sleep(0.2)
        else:
            break
    with open(path, "r") as stream:
        js = json.load(stream)
        if js['success']:
            data_zero = js['data']['data_0']
            # find id
            id_user = None
            for d in data_zero:
                try:
                    id_user = int(d)
                except:
                    continue
            if id_user is None:
                error = js['data']['data_0']
                return False, error
            return True, id_user
        else:
            error = js['data']['data_0']
            return False, error


def redirect_to_result_page(id, task_type, result_file_path, task_subtype):
    if task_type == "get":
        if task_subtype == "selection" or task_subtype == "dataset":
            buttons = []
            if task_subtype == "selection":
                buttons.append(["Selection info", os.path.basename(result_file_path), "selection_info.json"])
            else:
                if str(result_file_path).endswith(".sdf"):
                    buttons.append(["SDF file", os.path.basename(result_file_path), "molecules.sdf"])
                else:
                    buttons.append(["Data-set info file", os.path.basename(result_file_path), "data_set_info.sdf"])
            return render_template("result_get_one.html", DOWNLOADS=buttons)
        else:
            with open(result_file_path, "r") as stream:
                js = json.load(stream)
            data = js['data']
            parse_data = []
            for key in data:
                parse_data.append(data[key])
            metadata = []
            for key in js:
                if not key == "data" and not key == "column_names":
                    metadata.append([key, js[key]])
            if len(parse_data) > 1:
                # x result
                clean_temp(id)
                return render_template('result_get_x.html', FIELDS=metadata, HEADERS_TABLE=js['column_names'],
                                       RESULTS_TABLE=parse_data)
            else:
                if len(parse_data) == 0:
                    metadata.append(["Result", "No result match"])
                else:
                    # one result
                    pst = -1
                    for item in js['column_names']:
                        pst += 1
                        try:
                            metadata.append([item, parse_data[0][pst]])
                        except:
                            continue
                clean_temp(id)
                return render_template('result.html', FIELDS=metadata)
    elif task_type == "put":
        with open(result_file_path, "r") as stream:
            js = json.load(stream)
            rows = []
            for key in js:
                rows.append([key, js[key]])
        clean_temp(id)
        return render_template('result.html', FIELDS=rows)
    # run type
    else:
        subtests, test_info = parse_run_result(result_file_path)
        # clean_temp(id)
        return render_template("result_run.html", SUBTESTS=subtests, FIELDS=test_info)
    return None


def parse_run_result(result_file_path):
    """
    Makes a result page for run task.

    :param test_id: ID of taken place test
    :return: template
    """
    error = None
    with open(result_file_path, "r") as stream:
        js = json.load(stream)
    success = js['success']
    if not success:
        return str("Test didn't succeed")
    data = js['data']
    test_info = []
    subtests = []
    for key in data.keys():
        if not str(key).startswith("subtest"):
            test_info.append(data[key])
        else:
            subtests.append(data[key])
    return subtests, test_info


@app.template_test(name="values_ending")
def ends_with_values(field):
    """
    Returns true if a given value ends with substring "values"
    :param field: Value
    :return: True/False
    """
    return str(field).endswith("values")


@app.template_test(name="names_ending")
def ends_with_names(field):
    """
    Returns true if a given value ends with substring "names"
    :param field: Value
    :return: True/False
    """
    return str(field).endswith("names")


def get_method(chosen_option):
    value = request.form['optionsRadios']
    value = str(value).replace("opt", "")
    array_of_options = GET_SCREEN[chosen_option]
    task_subtype = chosen_option
    name_of_filter = array_of_options[int(value) - 1][0]
    value_of_filter = request.form['filter_field']
    id = new_task_get(task_subtype, name_of_filter, value_of_filter)
    return render_template('id_of_query.html', ID=id)


def reading_run_options(form_dicitonary, files, user_id):
    dict = {}
    type = str(form_dicitonary['type_option']).replace("opt_", "")
    optim = str(form_dicitonary['optim_option']).replace("opt_", "")
    file = str(form_dicitonary['file_repr_option']).replace("opt_", "")
    sim = str(form_dicitonary['sim_option']).replace("opt_", "")
    repeat = str(form_dicitonary['repeat_box']).replace("opt_", "")
    parallel_selection = str(form_dicitonary['p_sels_option']).replace("opt_", "")

    dict['typefile'] = type
    try:
        val = int(repeat)
        if val == 0:
            val = 2
    except:
        val = 2
    dict['repeat'] = val
    dict['parallel_selections'] = parallel_selection
    dict['task_type'] = "run"
    dict['user'] = user_id
    if type == "python":
        # repr file
        if file == "upload_file":
            file = files['file_repr_inp_file']
            file_path = os.path.abspath(os.path.join(get_temp_path(), werkzeug.secure_filename(file.filename)))
            file.save(file_path)
            dict['repr_method'] = file_path
        else:
            # read id
            repr_method_id = str(form_dicitonary['file_repr_id_field']).replace("opt_", "")
            dict['repr_method'] = "&" + repr_method_id
        # sim file
        if sim == "upload_file":
            file = files['sim_inp_file']
            file_path = os.path.abspath(os.path.join(get_temp_path(), werkzeug.secure_filename(file.filename)))
            file.save(file_path)
            dict['sim_method'] = file_path
        else:
            # read id
            sim_method_id = str(form_dicitonary['sim_id_field']).replace("opt_", "")
            dict['sim_method'] = "&" + sim_method_id

        # optimization
        dict['optimalization'] = optim
    else:
        # file file
        if file == "upload_file":
            file = files['file_repr_inp_file']
            file_path = os.path.abspath(os.path.join(get_temp_path(), werkzeug.secure_filename(file.filename)))
            file.save(file_path)
            dict['file'] = file_path
        else:
            # read id
            file_id = str(form_dicitonary['file_repr_id_field']).replace("opt_", "")
            dict['file'] = "&" + file_id
        dict['execute_command'] = form_dicitonary['execute_comm_field']
    # d/s
    d_s = []
    for key in form_dicitonary:
        if str(key).startswith("d_s_input_"):
            values = str(form_dicitonary[key]).split("_")
            data_set = values[0]
            selection = values[1]
            d_s.append({
                "dataset": data_set,
                "selection": selection
            })
    dict["d/s"] = d_s
    return True, dict


def add_metacentrum_infos(form_dicitonary, config_dict):
    username = form_dicitonary['input_login']
    pas = form_dicitonary['input_password']
    if username == "" or pas == "":
        return False, config_dict
    else:
        path = create_mc_log_data(username, pas)
        config_dict['login_data'] = os.path.abspath(path)
        config_dict['metacentrum'] = "True"
        config_dict['front_ends_nodes'] = get_path_front_nodes(form_dicitonary['select_fen'])
        return True, config_dict


def add_rc_infos(form_dicitonary, config_dict):
    # remote pcs
    pcs = []
    for key in form_dicitonary:
        if str(key).startswith("pcs_input_"):
            values = str(form_dicitonary[key]).split("_")
            hostname = values[0]
            username = values[1]
            password = values[2]
            os_of_pc = values[3]
            version = values[4]
            pcs.append({
                "hostname": hostname,
                "username": username,
                "password": password,
                "OS_type": os_of_pc,
                "py_version": version}
            )
    config_dict["pc_to_use"] = create_rc_pcs_data(pcs)
    config_dict['parallel_pc'] = "True"
    return True, config_dict


def task_id():
    global i
    i += 1
    return str(i)


def get_task_information(id):
    global actual_tasks
    if id in actual_tasks:
        return actual_tasks[id]
    else:
        return None


def get_path(id, type):
    if type == "sdf":
        suffix = ".sdf"
    else:
        suffix = ".json"
    if type == "test":
        prefix = 'query_'
    elif type == "result":
        prefix = "result_"
    else:
        global l
        l += 1
        prefix = "t_file_" + str(l) + "_"
    file_name = prefix + str(id) + suffix
    path = os.path.join(get_temp_path(), file_name)
    return os.path.abspath(path)


def get_temp_path():
    path = 'temp'
    if not os.path.exists(path):
        os.mkdir(path)
    return path


def set_task_information(id, task_type, output_file, task_subtype=None, p=None):
    global actual_tasks
    actual_tasks[id] = [task_type, output_file, task_subtype, p]


def new_task_get(task_subtype, name_of_filter, value_of_filter, **args):
    """
    Starts a new task of type get.
    :param task_subtype:  Subtype of task.
    :param name_of_filter: Name of column in where clause
    :param value_of_filter: Value in where clause
    :param args: Another arguments
    :return: ID of task
    """
    task_type = "get"
    id = task_id()
    query_path = get_path(id, 'test')
    if task_subtype == "dataset" and "sdf_file" in args:
        result_path = get_path(id, "sdf")
    else:
        result_path = get_path(id, 'result')
    test_config = {'task_type': task_type,
                   'task_subtype': task_subtype,
                   'output_file': result_path,
                   name_of_filter: value_of_filter}
    for key in args:
        test_config[key] = args[key]
    p = new_test(test_config, query_path)
    set_task_information(id, task_type, result_path, task_subtype, p=p)
    return id


def new_task_put(task_subtype, **args):
    task_type = "put"
    id = task_id()
    query_path = get_path(id, 'test')
    result_path = get_path(id, 'result')
    test_config = {'task_type': task_type,
                   'task_subtype': task_subtype,
                   'output_file': result_path}
    for key in args:
        test_config[key] = args[key]
    p = new_test(test_config, query_path)
    set_task_information(id, task_type, result_path, p=p)
    return id


def new_test(test_config, input_path):
    c_d = {"test": {"n1": test_config},
           "metadata": {
               "test_count": 1,
               "ignore": [],
               "parallel_tests": "False"
           }}
    with open(input_path, "w") as stream:
        json.dump(c_d, stream, indent=2)
    line_arguments = {
        "mode_a": True,
        "file": input_path,
        "mode": "a"
    }
    p = multiprocessing.Process(target=vsb_app.main, args=(line_arguments,))
    p.start()
    return p


def new_task_run(dictionary):
    id = task_id()
    task_type = dictionary['task_type']
    query_path = get_path(id, 'test')
    result_path = get_path(id, 'result')
    dictionary['fc_ofile'] = result_path
    p = new_test(dictionary, query_path)
    set_task_information(id, task_type, result_path, p=p)
    return id


def is_end_of_task(id):
    infs = get_task_information(id)
    if infs is None:
        return False, "Task doesn't exist"
    p = infs[3]
    if p.exitcode is None:
        return False, "Task is still running"
    else:
        return True, "OK"


def get_front_nodes():
    names = []
    with open("data/front_nodes.json", "r") as stream:
        js = json.load(stream)
        for node in js['nodes']:
            names.append(node['name'])
    return names


def get_path_front_nodes(name):
    d = {}
    with open("data/front_nodes.json", "r") as stream:
        js = json.load(stream)
        for node in js['nodes']:
            if node['name'] == name:
                d['nodes'] = [node]
    t_file = get_path(0, "file")
    with open(t_file, "w") as stream:
        json.dump(d, stream, indent=2)
    return t_file


def create_mc_log_data(username, password):
    t_file = get_path(0, "t")
    dict = {"metacentrum": {
        "login": username,
        "password": password
    }
    }
    with open(t_file, "w") as stream:
        json.dump(dict, stream)
    return t_file


def create_rc_pcs_data(pcs):
    t_file = get_path(0, "t")
    dict = {"pcs": pcs}
    with open(t_file, "w") as stream:
        json.dump(dict, stream)
    return t_file


def allowed_file(filename):
    ALLOWED_EXTENSIONS = ['json', 'sdf']
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def test_init():
    set_task_information("1", "run", os.path.abspath(os.path.join(get_temp_path(), "result_" + str(1) + ".json")))
    set_task_information("2", "get", os.path.abspath(os.path.join(get_temp_path(), 't_file_2_2.sdf')), "dataset")


def _read_config(path="configuration/conf.json"):
    global config
    with open(path, "r") as stream:
        config = json.load(stream)


def clean_temp(id):
    query_path = get_path(id, "test")
    result_path = get_path(id, "result")
    try:
        os.remove(query_path)
    except Exception as e:
        pass
    try:
        os.remove(result_path)
    except Exception as e:
        pass


if __name__ == "__main__":
    # test_init()
    print("Reading configuration")
    _read_config()
    print("Importing VSB application")
    try:
        full_path = os.path.abspath(config['vsb_path'])
        sys.path.append(os.path.dirname(full_path))
        vsb_app = importlib.import_module(os.path.basename(full_path).replace(".py", ""))
    except Exception as e:
        print("VSB application couldn't be imported: {0}".format(e))
        print("Web pages are shut down")
        exit()
    try:
        importlib.invalidate_caches()
    except:
        print('Lower python version')
    app.run()
